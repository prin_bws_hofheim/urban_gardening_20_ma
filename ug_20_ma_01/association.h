#ifndef ASSOCIATION_H
#define ASSOCIATION_H

#include <vector>
#include "patch.h"
#include "member.h"

using namespace std;

class Association
{
public:

    static const unsigned short maxCountPatchesMembers = 100;
    Association();
    bool leasePatch(int patchId, int memberId);
    bool handleApplicationLeasePatch(int memberId, int boardMemberId, bool confirm);
    bool releasePatch(int patchId);
    void showPatchesAll(vector<Patch *> &tempPatches) const;
    bool showPatchSingle(int id, int &patchId, int &fieldId, unsigned short &quality) const;
    void showMembersAll(vector<Member *> &tempMember) const;

    bool updateMember(QString name, int memberId, bool board, Patch *patch);
    bool updatePatch(int patchId, int fieldId, unsigned short quality);

    bool findPatch(int patchId);
    void testMember1();
private:

    Patch *patches[maxCountPatchesMembers];
    Member *members[maxCountPatchesMembers];
    Member member;
};

#endif // ASSOCIATION_H
