#include "patch.h"

Patch::Patch()
{
    patchId = -1;
    field =  -1;
    quality = 0;
    statusPatch = statusUnknown;
}

Patch::Patch(int newPatchId, int newField, unsigned short newQuality, statusPatchType newStatusPatch)
{
    patchId = newPatchId;
    field =  newField;
    quality = newQuality;
    statusPatch = newStatusPatch;
}

int Patch::getPatchId()
{
    return patchId;
}

void Patch::setPatchId(int newPatchId)
{
    patchId = newPatchId;
}

int Patch::getField()
{
    return field;
}

void Patch::setField(int newfield)
{
    field = newfield;
}

unsigned short Patch::getQuality()
{
    return quality;
}

void Patch::setQuality(unsigned short newquality)
{
    quality = newquality;
}

Patch::statusPatchType Patch::getStatus()
{
    return statusPatch;
}

void Patch::setStatus(statusPatchType newStatusPatch)
{
    statusPatch = newStatusPatch;
}
