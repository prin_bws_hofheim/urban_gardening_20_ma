#ifndef MEMBER_H
#define MEMBER_H

#include <QString>
#include "patch.h"

class Member
{
public:
    enum leaseRequestType{leaseRequestNone, leaseRequestRequestet, leaseRequestConfirmed, leaseRequestDenied, leaseRequestUnknown = -1};
    Member();
    Member(int newMemberId, QString newName, QString newPassword, bool newBoardMember, Patch *newPatch, leaseRequestType newLeaseRequest);

    QString getName();
    void setName(QString newName);

    int getMemberId();
    void setMemberId(int newMemberId);

    bool getIsBoardMember();
    void setIsBoardMember(bool newIsBoardMember);

    Patch *getPatch() const;
    void setPatch(Patch *newPatch);

    leaseRequestType getLeaseRequest();
    void setLeaseRequest(leaseRequestType newLeaseRequest);

    static QString getLeaseRequestString(leaseRequestType newLeaseRequest);

    QString getPassword() const;
    void setPassword(const QString &newPassword);

private:
    QString name;
    QString password;
    int memberId;
    bool isBoardMember;
    Patch *patch;
    leaseRequestType leaseRequest;
};

#endif // MEMBER_H
