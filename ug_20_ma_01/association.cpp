#include "association.h"

Association::Association()
{
    for(int i = 0; i < Association::maxCountPatchesMembers; i++)
        patches[i] = new Patch;
    for(int i = 0; i < Association::maxCountPatchesMembers; i++)
        members[i] = new Member;

}

bool Association::leasePatch(int patchId, int memberId)
{
    Patch *tempPatch = nullptr;

    if (   memberId < 0
        || memberId >= maxCountPatchesMembers
        || patchId < 0
        || patchId >= maxCountPatchesMembers)
        return false;

    if(nullptr == members[memberId]->getPatch())
    {
        for(int i = 0; i < Association::maxCountPatchesMembers; i++)
            if(nullptr != members[i]->getPatch())
                if(members[i]->getPatch()->getPatchId() == patchId)
                    return false;

        tempPatch = patches[patchId];
        members[memberId]->setPatch(tempPatch);
        tempPatch->setStatus(Patch::statusRequested);
        return true;
    }
    return false;
}

bool Association::releasePatch(int patchId)
{
    Patch *tempPatch = nullptr;
    bool found = false;

    if (   patchId < 0
        || patchId >= maxCountPatchesMembers)
        return false;

    tempPatch = patches[patchId];
    for(int i = 0; i < maxCountPatchesMembers; i++)
    {
        if(members[i]->getPatch() == tempPatch)
        {
            members[i]->setPatch(nullptr);
            found = true;
        }
    }
    return found;
}

void Association::showPatchesAll(vector<Patch*> &tempPatches) const
{
    for(int i = 0; i < maxCountPatchesMembers; i++)
        tempPatches.push_back(patches[i]);
}

/**
 * @brief Association::showSinglePatch
 * @param Id Nr des geschuten Beets
 * @param patchId nr, als bestätigung
 * @param field Acker
 * @param quality Qualitätsstufe
 * @return war zu finden? Ja/Nein
 *
 * 1. Prüfen, ob Nr gültig ist.
 * 2. Objekt suchen
 * 3. Daten zurückgeben (als Reference)
 *
 */
bool Association::showPatchSingle(int id, int &patchId, int &fieldId, unsigned short &quality) const
{
    if (   patchId < 0
        || patchId >= maxCountPatchesMembers)
        return false;

    patchId = patches[id]->getPatchId();
    fieldId = patches[id]->getField();
    quality = patches[id]->getQuality();
    return true;
}

void Association::showMembersAll(vector<Member*> &tempMember) const
{
    for(int i = 0; i < maxCountPatchesMembers; i++)
        tempMember.push_back(members[i]);
}

bool Association::updateMember(QString name, int memberId, bool board, Patch *patch)
{
    if (   memberId < 0
        || memberId >= maxCountPatchesMembers)
        return false;

    members[memberId]->setName(name);
    members[memberId]->setMemberId(memberId);
    members[memberId]->setIsBoardMember(board);
    members[memberId]->setPatch(patch);
    return true;
}

bool Association::updatePatch(int patchId, int fieldId, unsigned short quality)
{
    if (   patchId < 0
        || patchId >= maxCountPatchesMembers)
        return false;

    patches[patchId]->setPatchId(patchId);
    patches[patchId]->setField(fieldId);
    patches[patchId]->setQuality(quality);
    return true;
}

bool Association::handleApplicationLeasePatch(int memberId, int boardMemberId,
                                          bool confirm)
{
    Patch *patch = nullptr;

    if(-1 == memberId)
        return false;

    if(!members[boardMemberId]->getIsBoardMember())
        return false;

    // Suche Beet
    //    if(0 <= patchNr && maxCountPatchesMembers > patchNr)
    //    {
    //        if(Beet::reserviert == beete[patchNr].getStatus())
    //        {
    //            if(true == bestaetigen)
    //            {
    //                beete[patchNr].setStatus(Beet::gepachtet);
    //            }
    //            else
    //            {
    //                beete[patchNr].setStatus(Beet::frei);
    //            }

    //        }
    //        return;
    //    }

    // Suche Mitglied
    if(0 <= memberId && maxCountPatchesMembers > memberId)
    {
        if(Member::leaseRequestRequestet == members[memberId]->getLeaseRequest())
        {
            patch = members[memberId]->getPatch();
            if(nullptr != patch)
            {
                if(true == confirm)
                {
                    patch->setStatus(Patch::statusLeased);
                    members[memberId]->setLeaseRequest(Member::leaseRequestConfirmed);
                    return true;
                }
                else
                {
                    patch->setStatus(Patch::statusFree);
                    members[memberId]->setLeaseRequest(Member::leaseRequestDenied);
                    return false;
                }
            }
        }
    }
    return false;
}

void Association::testMember1()
{
    member.getMemberId();
}
