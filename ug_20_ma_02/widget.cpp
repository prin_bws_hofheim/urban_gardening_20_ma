#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    ug20 = new Association;
    myTestMemersPatches = new TestCreateMembersPatches(ug20);

    connect(ui->btnShowSinglePatch, &QPushButton::clicked, this, &Widget::showSinglePatch);
    connect(ui->btnCreateTestSet1, &QPushButton::clicked, this, &Widget::createTestSet1);
    connect(ui->btnShowPatchesAll, &QPushButton::clicked, this, &Widget::showPatchesAll);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::showSinglePatch()
{
    int patchIdIn = -1;
    int patchId = -1;
    int fieldId = -1;
    unsigned short quality = 0;

    patchIdIn = ui->spinPatchIn->value();

    ug20->showPatchSingle(patchIdIn, patchId, fieldId, quality);

    ui->spinPatchId->setReadOnly(true);
    ui->spinFieldId->setReadOnly(true);
    ui->spinQuality->setReadOnly(true);

    ui->spinPatchId->setValue(patchId);
    ui->spinFieldId->setValue(fieldId);
    ui->spinQuality->setValue(quality);
}


void Widget::createTestSet1()
{
    myTestMemersPatches->testgenerateValidePatches();
}

void Widget::showPatchesAll()
{
    vector<Patch*> tempPatches;
    QTableWidgetItem* tempItem = nullptr;

    ug20->showPatchesAll(tempPatches);

    ui->tableWidgetPatches->clearContents();
    ui->tableWidgetPatches->setRowCount(tempPatches.size());
    for(unsigned long i = 0; i < tempPatches.size(); i++) {
        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempPatches[i]->getPatchId()));
        ui->tableWidgetPatches->setItem(i, 0, tempItem);
        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempPatches[i]->getField()));
        ui->tableWidgetPatches->setItem(i, 1, tempItem);
        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempPatches[i]->getQuality()));
        ui->tableWidgetPatches->setItem(i, 2, tempItem);
    }
}

