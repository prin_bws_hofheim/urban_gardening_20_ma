/**
 * @main Komanduzeilenargument: g für GUI, k für Konsole.
 * Konsole funktioniert in Windows in diesem Programm nicht!
 */

#include <QApplication>
#include <iostream>
#include <string>

#include "widget.h"

using namespace std;

/**
 * main-Funktion mit Schalter Konsole / GUI
 * Schalter funktioniert bei Windows nur auf GUI.
 * Konolen-Main für Windows ist main-konsole.cpp, im Projektbaum
 * Alle Klasen bleiben identisch.
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    int retCode = -1;
        w.show();
        retCode = a.exec();
    return retCode;
}
