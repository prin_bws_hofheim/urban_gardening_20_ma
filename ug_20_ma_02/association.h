#ifndef ASSOCIATION_H
#define ASSOCIATION_H

#include <vector>
#include "patch.h"

using namespace std;

class Association
{
public:

    static const unsigned short maxCountPatchesMembers = 100;
    Association();
    void showPatchesAll(vector<Patch *> &tempPatches) const;
    bool showPatchSingle(int id, int &patchId, int &fieldId, unsigned short &quality) const;
    bool updatePatch(int patchId, int fieldId, unsigned short quality);

    bool findPatch(int patchId);
private:

    Patch *patches[maxCountPatchesMembers];
};

#endif // ASSOCIATION_H
