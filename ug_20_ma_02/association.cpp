#include "association.h"

Association::Association()
{
    for(int i = 0; i < Association::maxCountPatchesMembers; i++)
        patches[i] = new Patch;
}


void Association::showPatchesAll(vector<Patch*> &tempPatches) const
{
    for(int i = 0; i < maxCountPatchesMembers; i++)
        tempPatches.push_back(patches[i]);
}

/**
 * @brief Association::showSinglePatch
 * @param Id Nr des geschuten Beets
 * @param patchId nr, als bestätigung
 * @param field Acker
 * @param quality Qualitätsstufe
 * @return war zu finden? Ja/Nein
 *
 * 1. Prüfen, ob Nr gültig ist.
 * 2. Objekt suchen
 * 3. Daten zurückgeben (als Reference)
 *
 */
bool Association::showPatchSingle(int id, int &patchId, int &fieldId, unsigned short &quality) const
{
    if (   patchId < 0
        || patchId >= maxCountPatchesMembers)
        return false;

    patchId = patches[id]->getPatchId();
    fieldId = patches[id]->getField();
    quality = patches[id]->getQuality();
    return true;
}

bool Association::updatePatch(int patchId, int fieldId, unsigned short quality)
{
    if (   patchId < 0
        || patchId >= maxCountPatchesMembers)
        return false;

    patches[patchId]->setPatchId(patchId);
    patches[patchId]->setField(fieldId);
    patches[patchId]->setQuality(quality);
    return true;
}

