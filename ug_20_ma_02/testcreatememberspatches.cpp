#include "testcreatememberspatches.h"


TestCreateMembersPatches::TestCreateMembersPatches(Association *newAssociation)
{
    ug20Tester = newAssociation;
}


bool TestCreateMembersPatches::testgenerateValidePatches()
{
    if(nullptr != ug20Tester)
    {
        for(int id = 0; id < ug20Tester->maxCountPatchesMembers; id++ )
        {
            switch(id)
            {
            case 0:
                ug20Tester->updatePatch(0, 0, 1);
                break;
            case 1:
                ug20Tester->updatePatch(10, 1, 3);
                break;
            case 2:
                ug20Tester->updatePatch(1, 0, 1);
                break;
            case 3:
                ug20Tester->updatePatch(11, 1, 3);
                break;
            case 4:
                ug20Tester->updatePatch(2, 0, 1);
                break;
            case 5:
                ug20Tester->updatePatch(12, 1, 3);
                break;
            case 6:
                ug20Tester->updatePatch(3, 0, 1);
                break;
            case 7:
                ug20Tester->updatePatch(13, 1, 3);
                break;
            case 8:
                ug20Tester->updatePatch(4, 0, 1);
                break;
            case 9:
                ug20Tester->updatePatch(14, 1, 3);
                break;
            case 10:
                ug20Tester->updatePatch(5, 0, 1);
                break;
            case 11:
                ug20Tester->updatePatch(15, 1, 3);
                break;
            case 12:
                ug20Tester->updatePatch(6, 0, 1);
                break;
            case 13:
                ug20Tester->updatePatch(20, 3, 5);
                break;
            case 14:
                ug20Tester->updatePatch(7, 0, 1);
                break;
            case 15:
                ug20Tester->updatePatch(21, 3, 5);
                break;
            case 16:
                ug20Tester->updatePatch(8, 0, 1);
                break;
            case 17:
                ug20Tester->updatePatch(25, 3, 5);
                break;
            case 18:
                ug20Tester->updatePatch(9, 0, 1);
                break;
            case 19:
                ug20Tester->updatePatch(27, 3, 5);
                break;
            case 20:
                ug20Tester->updatePatch(16, 4, 2);
                break;
            case 21:
                ug20Tester->updatePatch(17, 4, 2);
                break;
            case 22:
                ug20Tester->updatePatch(18, 4, 2);
                break;
            case 23:
                ug20Tester->updatePatch(19, 4, 2);
                break;
            case 24:
                ug20Tester->updatePatch(22, 6, 1);
                break;
            case 25:
                ug20Tester->updatePatch(24, 6, 1);
                break;
            case 26:
                ug20Tester->updatePatch(28, 6, 1);
                break;
            case 27:
                ug20Tester->updatePatch(23, 6, 1);
                break;
            case 28:
                ug20Tester->updatePatch(29, 6, 1);
                break;
            case 29:
                ug20Tester->updatePatch(26, 6, 1);
                break;
            case 30:
                break;
            case 31:
                break;
            case 32:
                break;
            case 33:
                break;
            case 34:
                break;
            case 35:
                break;
            case 36:
                break;
            case 37:
                break;
            case 38:
                break;
            case 39:
                break;
            case 40:
                break;
            case 41:
                break;
            case 42:
                break;
            case 43:
                break;
            case 44:
                break;
            case 45:
                break;
            case 46:
                break;
            case 47:
                break;
            case 48:
                break;
            case 49:
                break;
            case 50:
                break;
            case 51:
                break;
            case 52:
                break;
            case 53:
                break;
            case 54:
                break;
            case 55:
                break;
            case 56:
                break;
            case 57:
                break;
            case 58:
                break;
            case 59:
                break;
            case 60:
                break;
            case 61:
                break;
            case 62:
                break;
            case 63:
                break;
            case 64:
                break;
            case 65:
                break;
            case 66:
                break;
            case 67:
                break;
            case 68:
                break;
            case 69:
                break;
            case 70:
                break;
            case 71:
                break;
            case 72:
                break;
            case 73:
                break;
            case 74:
                break;
            case 75:
                break;
            case 76:
                break;
            case 77:
                break;
            case 78:
                break;
            case 79:
                break;
            case 80:
                break;
            case 81:
                break;
            case 82:
                break;
            case 83:
                break;
            case 84:
                break;
            case 85:
                break;
            case 86:
                break;
            case 87:
                break;
            case 88:
                break;
            case 89:
                break;
            case 90:
                break;
            case 91:
                break;
            case 92:
                break;
            case 93:
                break;
            case 94:
                break;
            case 95:
                break;
            case 96:
                break;
            case 97:
                break;
            case 98:
                break;
            case 99:
                break;
            }
        }
    }
    return true;
}


