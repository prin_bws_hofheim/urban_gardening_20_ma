#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QInputDialog>
#include <QMessageBox>
#include "association.h"
#include "testcreatememberspatches.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void showSinglePatch();
    void createTestSet1();
    void showPatchesAll();

private:
    Ui::Widget *ui;
    Association *ug20;
    TestCreateMembersPatches *myTestMemersPatches;
};
#endif // WIDGET_H
